#ifndef MG_OCTAVENOISEFIELD_H
#define MG_OCTAVENOISEFIELD_H

#include "aggregatefield.h"
#include <memory>

class SimplexNoiseField;

class OctaveNoiseField : public AbstractField
{
public:
    OctaveNoiseField(int octaves, int seed);

    double falloff() const;
    void setFalloff(double falloff);

    double inputScale() const;
    void setInputScale(double iscale);

    double outputScale() const;
    void setOutputScale(double oscale);

    double outputOffset() const;
    void setOutputOffset(double offset);

    double valueAt(double X, double Y) const;

private:
    SimplexNoiseField* first() const;

    double _falloff;
    std::auto_ptr<AggregateField> sum;
};

#endif
