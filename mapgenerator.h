#ifndef MAPGENERATOR_H
#define MAPGENERATOR_H

#define MAP_SIZE 512

#include <QImage>
#include <QPoint>
class AggregateField;
class AbstractField;

enum BiomeType {
    Ocean,
    Forest,
    Snow,
    Plains,
    Desert,
    Beach,
    Bare,
    Lake,
    River,
    Cliffs
};

class MapGenerator
{
public:
    MapGenerator(int _seed, int numContinents = 3);
    ~MapGenerator();

    static QRgb distanceColor(int dist, bool water = false);
    static int colorDistance(QRgb rgb);

    int seed() const;
    QImage map() const;
    void saveMap(const QString& filename);

    QImage debugOverlay() const;
    QImage slopeMap(bool blend = false) const;
    QImage moistureMap(bool quantize = false) const;
    QImage temperatureMap(bool quantize = false) const;
    QImage biomeMap(bool slope = false, bool blend = false) const;

    enum TileType {
        TT_Ocean,
        TT_River,
        TT_Lake,
        TT_Land
    };
    int pixelAt(int x, int y) const;
    int temperatureAt(int x, int y) const;
    int moistureAt(int x, int y) const;
    int biomeAt(int x, int y) const;
    int tileAt(int x, int y) const;
    float altitudeAt(int x, int y) const;

private:
    void generateMountains();
    void createMoistureNoise();
    void generateNoise();
    void fillOcean(int x0 = 0, int y0 = 0, int state = 0);
    int fillLake(int x0, int y0, float height, int state = 0, int x00 = -1, int y00 = -1);
    void fillDistance();
    void calculateSlopes();
    void addRivers();
    void addRiver(int x, int y);
    void calculateMoisture();

    int _seed, numContinents, exclusionRadius;
    QImage img;
    AggregateField* mountains;
    AggregateField* noiseField;
    AbstractField* extraNoise;

    QVector<float> altitude, moisture, moistureNoise;
    QVector<QPoint> lakeExits, lakeExitPoints, riverStarts;
    QVector<signed char> slope;
    QVector<signed short> lakeSize;
    std::vector<std::vector<int>> ranges;
};

#endif
