#include "pointdistancefield.h"

PointDistanceField::PointDistanceField(double X, double Y) : _X(X), _Y(Y), _oscale(1.0), _threshold(255.0)
{
    // initializers only
}

double PointDistanceField::outputScale() const
{
    return _oscale;
}

void PointDistanceField::setOutputScale(double oscale)
{
    _oscale = oscale;
}

double PointDistanceField::outputThreshold() const
{
    return _threshold;
}

void PointDistanceField::setOutputThreshold(double threshold)
{
    _threshold = threshold;
}

double PointDistanceField::valueAt(double X, double Y) const 
{
    double dx = (X-_X), dy = (Y-_Y);
    double rv = _oscale / (dx*dx + dy*dy);
    if(rv > _threshold)
        return _threshold;
    return rv;
}
