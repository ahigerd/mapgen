#ifndef MG_SIMPLEXNOISEFIELD_H
#define MG_SIMPLEXNOISEFIELD_H

#include "abstractfield.h"

class SimplexNoiseField : public AbstractField
{
public:
    SimplexNoiseField(int seed);

    int seed() const;

    double inputScale() const;
    void setInputScale(double iscale);

    double outputScale() const;
    void setOutputScale(double oscale);

    double outputOffset() const;
    void setOutputOffset(double offset);

    double valueAt(double X, double Y) const;

private:
    int _seed;
    double _iscale, _oscale, _offset;
};

#endif
