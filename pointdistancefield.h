#ifndef MG_POINTDISTANCEFIELD_H
#define MG_POINTDISTANCEFIELD_H

#include "abstractfield.h"

class PointDistanceField : public AbstractField
{
public:
    PointDistanceField(double _X, double _Y);

    double outputScale() const;
    void setOutputScale(double oscale);

    double outputThreshold() const;
    void setOutputThreshold(double threshold);

    double valueAt(double X, double Y) const;

private:
    double _X, _Y, _oscale, _threshold;
};

#endif
