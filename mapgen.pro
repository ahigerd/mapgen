#CONFIG += qxt debug
#QXT = core web
QT = core widgets
QMAKE_CXXFLAGS += -std=c++11 -gdwarf-2
OBJECTS_DIR = .build
MOC_DIR = .build

HEADERS += abstractfield.h aggregatefield.h octavenoisefield.h simplexnoisefield.h pointdistancefield.h linedistancefield.h
SOURCES += aggregatefield.cpp octavenoisefield.cpp simplexnoisefield.cpp pointdistancefield.cpp linedistancefield.cpp

HEADERS += mapgenerator.h   tilemap.h   quadtree.h
SOURCES += mapgenerator.cpp tilemap.cpp quadtree.cpp test.cpp

# HEADERS += mapservice.h
# SOURCES += mapservice.cpp
