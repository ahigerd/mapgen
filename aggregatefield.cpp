#include "aggregatefield.h"

AggregateField::AggregateField() : _autoDelete(true)
{
    // initializers only
}

AggregateField::~AggregateField()
{
    if(_autoDelete) {
        int ct = children.size();
        for(int i = 0; i < ct; i++) {
            delete children[i];
        }
    }
}

bool AggregateField::autoDelete() const
{
    return _autoDelete;
}

void AggregateField::setAutoDelete(bool on)
{
    _autoDelete = on;
}

void AggregateField::add(AbstractField* field)
{
    children.push_back(field);
}
