#include "simplexnoisefield.h"
#include <cmath>

static int grad3[][3] = {{1,1,0},{-1,1,0},{1,-1,0},{-1,-1,0},{1,0,1},{-1,0,1},{1,0,-1},{-1,0,-1},{0,1,1},{0,-1,1},{0,1,-1},{0,-1,-1}};

inline static int fastFloor(double x)
{
    return (x>0) ? (int)x : (int)x-1;
}

inline static double dot(int g, double x, double y)
{
    return grad3[g][0]*x + grad3[g][1]*y; 
}

int AbstractField::hashFunction(int X, int Y, int seedToUse)
{
    // based on Jenkins96 hash
    int a = (int)(0x9E3779B9 + X);
    int b = (int)(0x9E3779B9 + Y);
    int c = seedToUse;

    a -= b; a -= c; a ^= (c>>13); 
    b -= c; b -= a; b ^= (a<<8); 
    c -= a; c -= b; c ^= (b>>13); 
    a -= b; a -= c; a ^= (c>>12);  
    b -= c; b -= a; b ^= (a<<16); 
    c -= a; c -= b; c ^= (b>>5); 
    a -= b; a -= c; a ^= (c>>3);  
    b -= c; b -= a; b ^= (a<<10); 
    c -= a; c -= b; c ^= (b>>15); 

    c += 12;

    a -= b; a -= c; a ^= (c>>13);
    b -= c; b -= a; b ^= (a<<8);
    c -= a; c -= b; c ^= (b>>13);
    a -= b; a -= c; a ^= (c>>12);
    b -= c; b -= a; b ^= (a<<16);
    c -= a; c -= b; c ^= (b>>5);
    a -= b; a -= c; a ^= (c>>3);
    b -= c; b -= a; b ^= (a<<10);
    c -= a; c -= b; c ^= (b>>15);

    return c & 0x7FFFFFFF;
}

SimplexNoiseField::SimplexNoiseField(int seed) : _seed(seed), _iscale(1.0), _oscale(1.0), _offset(0.0)
{
    // initializers only
}

int SimplexNoiseField::seed() const
{
    return _seed;
}

double SimplexNoiseField::inputScale() const
{
    return _iscale;
}

void SimplexNoiseField::setInputScale(double iscale)
{
    _iscale = iscale;
}

double SimplexNoiseField::outputScale() const
{
    return _oscale;
}

void SimplexNoiseField::setOutputScale(double oscale)
{
    _oscale = oscale;
}

double SimplexNoiseField::outputOffset() const
{
    return _offset;
}

void SimplexNoiseField::setOutputOffset(double offset)
{
    _offset = offset;
}

double SimplexNoiseField::valueAt(double X, double Y) const 
{
    double n0, n1, n2;
    int i1, j1; // Offsets for middle corner

    X *= _iscale;
    Y *= _iscale;

    //skew input space
    double F2 = 0.5*(std::sqrt(3.0)-1.0);
    double s = (X+Y)*F2;
    int i = fastFloor(X+s);
    int j = fastFloor(Y+s);
    
    double G2 = (3.0-std::sqrt(3.0))/6.0;
    double t  = (i+j)*G2;
    double X0 = i-t;  // unskew
    double Y0 = j-t;
    double x0 = X-X0; // x,y distance from cell origin
    double y0 = Y-Y0;
    
    if (x0>y0) {
        // lower triangle order: (0,0)->(1,0)->(1,1)
        i1 = 1; 
        j1 = 0;
    } else {
        // upper triangle order: (0,0)->(0,1)->(1,1)
        i1 = 0; 
        j1 = 1;
    } 
    
    double x1 = x0-i1+G2; // middle corner offsets unskewed
    double y1 = y0-j1+G2;
    double x2 = x0-1.0+2.0*G2; // last corner offsets unskewed
    double y2 = y0-1.0+2.0*G2;
    
    //hash stuff
    int gi0 = hashFunction(i,j,_seed)%12;
    int gi1 = hashFunction(i+i1,j+j1,_seed)%12;
    int gi2 = hashFunction(i+1,j+1,_seed)%12;
    
    //calculate contributions from 3 corners
    double t0 = 0.5-x0*x0-y0*y0;
    if(t0 < 0) {
        n0 = 0.0;
    } else {
        t0 *= t0;
        n0 = t0*t0*dot(gi0,x0,y0);
    }
    
    double t1 = 0.5-x1*x1-y1*y1;
    if(t1 < 0) {
        n1 = 0.0;
    } else {
        t1 *= t1;
        n1 = t1*t1*dot(gi1,x1,y1);
    }
    
    double t2 = 0.5-x2*x2-y2*y2;
    if(t2 < 0) {
        n2=0.0;
    } else {
        t2 *= t2;
        n2 = t2*t2*dot(gi2,x2,y2);
    }
    
    return 70.0 * (n0+n1+n2) * _oscale + _offset;
}
