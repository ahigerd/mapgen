#ifndef MG_ABSTRACTFIELD_H
#define MG_ABSTRACTFIELD_H

class AbstractField
{
public:
    static int hashFunction(int X, int Y, int seedToUse);

    virtual ~AbstractField() {}
    virtual double valueAt(double X, double Y) const = 0;
};

#endif
