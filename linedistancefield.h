#ifndef MG_LINEDISTANCEFIELD_H
#define MG_LINEDISTANCEFIELD_H

#include "abstractfield.h"

class LineDistanceField : public AbstractField
{
public:
    LineDistanceField(double X1, double Y1, double X2, double Y2);

    double outputScale() const;
    void setOutputScale(double oscale);

    double outputThreshold() const;
    void setOutputThreshold(double threshold);

    double valueAt(double X, double Y) const;

private:
    double _X1, _Y1, _X2, _Y2, _oscale, _threshold;
    double _dx, _dy, _lenSquared;
};

#endif
