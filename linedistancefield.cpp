#include "linedistancefield.h"

LineDistanceField::LineDistanceField(double X1, double Y1, double X2, double Y2)
: _X1(X1), _Y1(Y1), _X2(X2), _Y2(Y2), _oscale(1.0), _threshold(255.0)
{
    _dx = _X2 - _X1;
    _dy = _Y2 - _Y1;
    _lenSquared = _dx*_dx + _dy*_dy;
}

double LineDistanceField::outputScale() const
{
    return _oscale;
}

void LineDistanceField::setOutputScale(double oscale)
{
    _oscale = oscale;
}

double LineDistanceField::outputThreshold() const
{
    return _threshold;
}

void LineDistanceField::setOutputThreshold(double threshold)
{
    _threshold = threshold;
}

double LineDistanceField::valueAt(double X, double Y) const 
{
    double rv; 

    double x1 = (X - _X1), y1 = (Y - _Y1);
    double t = 0;
    if(_lenSquared) t = (x1 * _dx + y1 * _dy) / _lenSquared;
    if(t <= 0.0) {
        rv = _oscale / (x1*x1 + y1*y1);
    } else if(t >= 1.0) {
        double x2 = (X - _X2), y2 = (Y - _Y2);
        rv = _oscale / (x2*x2 + y2*y2);
    } else {
        double x = _X1 + t*_dx, y = _Y1 + t*_dy;
        double dx = X - x, dy = Y - y;
        rv = _oscale / (dx*dx + dy*dy);
    }

    if(rv > _threshold)
        return _threshold;
    return rv;
}
