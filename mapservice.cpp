#include "mapservice.h"
#include "mapgenerator.h"
#include "tilemap.h"
#include <QxtAbstractWebSessionManager>
#include <QxtWebEvent>
#include <QColor>
#include <QBuffer>
#include <QThread>
#include <cmath>
#include <time.h>

template<typename T>
class LambdaThread : public QThread
{
public:
    LambdaThread(T _fn) : fn(_fn) {}

    T fn;

    void run() { fn(); }
};

template<typename T>
void runOnThread(T _fn, QObject* receiver, const char* slot)
{
    auto thread = new LambdaThread<T>(_fn);
    QObject::connect(thread, SIGNAL(finished()), receiver, slot);
    QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()), Qt::QueuedConnection);
    thread->start(QThread::LowestPriority);
}

MapService::World::~World() {
    if(map) delete map;
    map = nullptr;
}

MapService::MapService(int /* sessionID */, QxtAbstractWebSessionManager* sm)
: QxtAbstractWebService(sm, sm)
{
    //QObject::connect(&expiryTimer, SIGNAL(timeout()), this, SLOT(expireWorlds()));
    //expiryTimer.start(3000);
}

void MapService::pageRequestedEvent(QxtWebRequestEvent* event)
{
    QString sSeed = event->url.queryItemValue("seed");
    int seed = 0;
    if(sSeed.isEmpty()) {
        seed = ::time(NULL);
    } else {
        bool ok = false;
        seed = sSeed.toInt(&ok, 0);
        if(!ok) {
            int ct = sSeed.length();
            for(int i = 0; i < ct; i++) {
                int shifted = (seed & 0xFF000000) >> 23;
                seed = ((seed << 8) | sSeed[i].unicode()) ^ shifted;
            }
        }
    }

    World& world = maps[seed];
    if(world.lastAccess == 0) {
        world.seed = seed;
        world.map = nullptr;
        runOnThread([&world, seed]{ world.map = new MapGenerator(seed); }, this, SLOT(dispatchPending()));
    }
    world.lastAccess = ::time(NULL);

    if(world.map) {
        dispatchRequest(seed, event->sessionID, event->requestID, event->url);
    } else {
        world.queue << World::QueuedRequest{ event->sessionID, event->requestID, event->url };
    }
}

void MapService::dispatchPending()
{
    for(World& world : maps) {
        if(!world.map) continue;
        QQueue<World::QueuedRequest> queue = world.queue;
        world.queue.clear();
        while(!queue.isEmpty()) {
            auto request = queue.dequeue();
            dispatchRequest(world.seed, request.sessionID, request.requestID, request.url);
        }
    } 
}

void MapService::dispatchRequest(int seed, int sessionID, int requestID, const QUrl& url)
{
    qDebug() << "Dispatching response to" << url << sessionID << requestID;
    World& world = maps[seed];
    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QString module = url.path().section("/", -1);
    QByteArray contentType;
    if(module == "map") {
        contentType = "image/png";
        if(world.mapImage.isNull())
            world.mapImage = world.map->biomeMap(true, true);
        world.mapImage.save(&buffer, "PNG");
    } else if(module == "tiles") {
        if(!world.tiles) {
            if(!world.tilesPending) {
                world.tilesPending = true;
                world.queue << World::QueuedRequest{ sessionID, requestID, url };
                runOnThread([&world]{ world.tiles = new TileMap(world.map); world.tilesPending = false; }, this, SLOT(dispatchPending()));
            }
            return;
        }
        contentType = "text/plain; charset=x-user-defined";
        buffer.write(world.tiles->tileData());
    }
    buffer.close();

    QxtWebPageEvent* page = new QxtWebPageEvent(sessionID, requestID, ba);
    page->contentType = contentType; 
    postEvent(page);
    qDebug() << "posted" << sessionID << requestID;
}

void MapService::expireWorlds()
{
    int expiry = ::time(NULL) - 9000;
    foreach(int seed, maps.keys()) {
        World& world = maps[seed];
        if(world.lastAccess < expiry && world.queue.isEmpty()) {
            delete world.map;
            world.map = nullptr;
            maps.remove(seed);
        }
    }
}
