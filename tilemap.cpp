#include "tilemap.h"
#include <cmath>
#include <QtDebug>

TileMap::Cell::Cell(BiomeType _biome, TileShape _shape) : biome(_biome), shape(_shape), below(nullptr)
{
    // initializers only
}

TileMap::Cell::~Cell()
{
    delete below;
    below = nullptr;
}

unsigned char TileMap::Cell::cliffBits() const
{
    unsigned char rv = (biome == Cliffs) ? 0 : bits;
    Cell* c = below;
    while(c) {
        rv |= c->bits;
        c = c->below;
    }
    return rv;
}

TileMap::TileMap(MapGenerator* map)
{
    for(int y = 1; y < MAP_SIZE-1; y++) {
        for(int x = 1; x < MAP_SIZE-1; x++) {
            int alt = std::pow(map->altitudeAt(x, y) / 60.0, 1.3) * 1.0;
            setTile(x-1, y-1, alt, (BiomeType)map->biomeAt(x, y));
        }
    }
    cleanEdges();
    calculateSteepness();
    fillCliffs();
}

void TileMap::setTile(int x, int y, int z, BiomeType biome, quint8 bits)
{
    if(x<0 || y<0 || x >= (MAP_SIZE-2) || y >= (MAP_SIZE-2)) return; // out of bounds
    if(biome == Ocean) return; // this is assumed
    if(layers.size() <= z) {
        QVector<Cell> xRow(MAP_SIZE-2, Cell());
        QVector<QVector<Cell>> xLayer(MAP_SIZE-2, xRow);
        QVector<qint16> xsRow(MAP_SIZE-2, -1);
        QVector<QVector<qint16>> xsLayer(MAP_SIZE-2, xsRow);
        while(layers.size() <= z) {
            layers << xLayer;
            flatness << xsLayer;
        }
    }

    if(flatness[z][y][x] == -1)
        flatness[z][y][x] = 0;
    if(z > 0)
        flatness[z-1][y][x] = 1;

    Cell* cell = &layers[z][y][x];
    while(cell->biome > biome && cell->below != nullptr) {
        cell = cell->below;
    }
    if(cell->biome > biome && cell->below == nullptr) {
        cell->below = new Cell();
        cell = cell->below;
    }
    if(cell->biome == biome) {
        cell->bits |= bits;
    } else {
        if(cell->biome != Ocean) {
            Cell* newCell = new Cell(cell->biome, cell->shape);
            newCell->below = cell->below;
            cell->below = newCell;
        }
        cell->biome = biome;
        cell->bits = bits;
    }
}

void TileMap::cleanEdges()
{
    int numLayers = layers.size();
    // yay copy-on-write data structures
    QVector<QVector<QVector<Cell>>> oldLayers = layers;
    for(int z = 0; z < numLayers - 1; z++) {
        auto& layer = oldLayers[z];
        for(int y = 1; y < MAP_SIZE-3; y++) {
            auto& row = layer[y];
            auto& prevRow = layer[y-1];
            auto& nextRow = layer[y+1];
            for(int x = 1; x < MAP_SIZE-3; x++) {
                auto& cell = row[x];
                if(cell.biome == River) continue;
                quint8 occurrences[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                occurrences[prevRow[x-1].biome]++;
                occurrences[prevRow[x].biome]++;
                occurrences[prevRow[x+1].biome]++;
                occurrences[row[x-1].biome]++;
                occurrences[row[x].biome]++;
                occurrences[row[x+1].biome]++;
                occurrences[nextRow[x-1].biome]++;
                occurrences[nextRow[x].biome]++;
                occurrences[nextRow[x+1].biome]++;
                if(occurrences[cell.biome] < 3) {
                    int maxBiome = Ocean;
                    quint8 maxNum = 0;
                    for(int i = 0; i < 9; i++) {
                        if(occurrences[i] > maxNum) {
                            maxNum = occurrences[i];
                            maxBiome = i;
                        }
                    }
                    if(cell.biome != maxBiome) {
                        layers[z][y][x].biome = (BiomeType)maxBiome;
                    }
                }
            }
        }
    }
    for(int z = 0; z < numLayers; z++) {
        auto& layer = layers[z];
        for(int y = 1; y < MAP_SIZE-3; y++) {
            auto& row = layer[y];
            for(int x = 1; x < MAP_SIZE-3; x++) {
                Cell* cell = &row[x];
                if(cell->biome == Ocean) continue;
                while(cell) {
                    if(cell->bits == TS_Solid) {
                        auto biome = cell->biome;
                        setTile(x-1, y-1, z, biome, B_SE);
                        setTile(x,   y-1, z, biome, B_SE | B_SW);
                        setTile(x+1, y-1, z, biome, B_SW);
                        setTile(x-1, y,   z, biome, B_SE | B_NE);
                        setTile(x+1, y,   z, biome, B_SW | B_NW);
                        setTile(x-1, y+1, z, biome, B_NE);
                        setTile(x,   y+1, z, biome, B_NE | B_NW);
                        setTile(x+1, y+1, z, biome, B_NW);
                    }
                    cell = cell->below;
                }
            }
        }
    }
}

void TileMap::calculateSteepness()
{
    bool didChange;
    int iter = 0;
    int numLayers = flatness.size();
    maxFlatness.resize(numLayers);
    int prevMax = 0;
    do {
        didChange = false;
        iter = prevMax + 1;
        qDebug() << "flatness" << iter;
        for(int z = 1; z < numLayers; z++) {
            auto& layer_s = flatness[z];
            qint16& maxFlat = maxFlatness[z];
            for(int y = 1; y < MAP_SIZE-3; y++) {
                auto& row_s = layer_s[y];
                for(int x = 1; x < MAP_SIZE-3; x++) {
                    qint16& flat = row_s[x];
                    if (flat != 0) continue;

                    for(int yi = -1; yi <= +1; yi++) {
                        for(int xi = -1; xi <= +1; xi++) {
                            if(yi == 0 && xi == 0) continue;
                            int here = layer_s[yi+y][xi+x];
                            if(here > 0 && here <= iter) {
                                int newSteep = (yi*yi) + (xi*xi) + 1 + here;
                                if(flat == 0 || newSteep < flat) {
                                    flat = newSteep;
                                    if(maxFlat < flat) {
                                        maxFlat = flat;
                                        if(prevMax < maxFlat)
                                            prevMax = maxFlat;
                                    }
                                }
                                didChange = true;
                            }
                        }
                    }
                }
            }
        }
    } while(didChange);
    for(int z = 1; z < numLayers; z++) {
        qDebug() << "layer" << z << "maxFlatness" << maxFlatness[z];
    }
}

void TileMap::fillCliffs()
{
    int numLayers = layers.size();
    for(int y = 0; y < MAP_SIZE-2; y++) {
        for(int x = 0; x < MAP_SIZE-2; x++) {
            QByteArray chunk;
            for(int z = 1; z < numLayers; z++) {
                Cell* cell = &layers[z][y+z < MAP_SIZE-2 ? y+z : y][x];
                char addBits = 0;
                unsigned char cliffBits = cell->cliffBits();
                if(cliffBits != TS_Solid) {
                    const Cell* above = &layers[z][y+z-1 < MAP_SIZE-2 ? y+z-1 : y][x];
                    unsigned char acb = above->cliffBits();
                    if(acb == TS_EdgeN) {
                        addBits = TS_Cliff5;
                    } else if(acb == TS_OuterNW) {
                        addBits = TS_Cliff6;
                    } else if(acb == TS_OuterNE) {
                        addBits = TS_Cliff4;
                    }
                }
                if(cliffBits != TS_Transparent && cliffBits != TS_Solid) {
                    addBits = cliffBits;
                }
                if(addBits) {
                    Cell* newCell = new Cell(cell->biome, cell->shape);
                    newCell->below = cell->below;
                    cell->below = newCell;
                    cell->bits = addBits;
                    cell->biome = Cliffs;
                }
            }
        }
    }
}

QByteArray TileMap::tileData() const
{
    QByteArray rv;
    // a megabyte should be enough in general, and it's much more efficient to overallocate than underallocate
    rv.reserve(1024*1024);
    int numLayers = layers.size();
    for(int y = 0; y < MAP_SIZE-2; y++) {
        for(int x = 0; x < MAP_SIZE-2; x++) {
            QByteArray chunk;
            int flat = 0;
            for(int z = 0; z < numLayers; z++) {
                int adj_y = y+z < MAP_SIZE-2 ? y+z : y;
                const Cell* c = &layers[z][adj_y][x];
                if(c->biome == Ocean) continue;
                flat = flatness[z][adj_y][x];
                while(c) {
                    chunk += char(c->biome) + 32;
                    chunk += c->bits + 32;
                    c = c->below;
                }
            }
            rv += char(chunk.length() / 2) + chunk + char((flat & 0x7F) + 32);
        }
    }
    return rv;
}
