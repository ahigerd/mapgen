#ifndef MG_AGGREGATEFIELD_H
#define MG_AGGREGATEFIELD_H

#include "abstractfield.h"
#include <vector>

class AggregateField : public AbstractField
{
public:
    template<typename Function>
    static AggregateField* create(Function function);

    ~AggregateField();

    std::vector<AbstractField*> children;

    bool autoDelete() const;
    void setAutoDelete(bool on);

    void add(AbstractField* field);

protected:
    AggregateField();

private:
    bool _autoDelete;
};

template<typename Function>
class FnAggregateField : public AggregateField
{
public:
    Function _combine;

    FnAggregateField(Function combine) : _combine(combine) {}

    double valueAt(double X, double Y) const {
        double rv = children[0]->valueAt(X, Y);
        int ct = children.size();
        if(ct == 1) return rv;
        for(int i = 1; i < ct; i++) {
            rv = _combine(rv, children[i]->valueAt(X, Y));
        }
        return rv;
    }
};

template<typename Function>
AggregateField* AggregateField::create(Function function)
{
    return new FnAggregateField<Function>(function);
}

#endif
