#include <QApplication>
#include <QTime>
#include <QImage>
#include <QLabel>
#include <QPixmap>
#include <QDebug>
#include <QFile>
#include <QDataStream>
#include <QPainter>
#include <algorithm>
#include <cmath>
#include <time.h>
#include "mapgenerator.h"
//#include "octavenoisefield.h"
//#include "quadtree.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv, false);

    int seed = ::time(NULL);
    int numContinents = 3;
    bool doSave = false;
    bool showDebug = false;

    QStringList args = app.arguments();
    args.removeFirst(); // argv[0] is the program name, not an argument
    foreach(const QString& arg, args) {
        if(arg == "-h" || arg == "-?" || arg == "--help" || arg == "/?") {
            qDebug() << "Usage: " << argv[0] << " [-d] [-s] [-c#] [seed]";
            qDebug() << "-d: Show debug overlay";
            qDebug() << "-s: Save generated map to map-#####.png";
            qDebug() << "-c: Generate # continents";
            qDebug() << "seed: Use the specified seed";
            return 0;
        } else if(arg == "-s") {
            doSave = true;
        } else if(arg == "-d") {
            showDebug = true;
        } else if(arg.left(2) == "-c") {
            numContinents = arg.mid(2).toInt();
        } else {
            bool ok = false;
            seed = arg.toInt(&ok, 0);
            if(!ok) {
                int ct = arg.length();
                for(int i = 0; i < ct; i++) {
                    int shifted = (seed & 0xFF000000) >> 23;
                    seed = ((seed << 8) | arg[i].unicode()) ^ shifted;
                }
                qDebug() << "Seed" << arg << "->" << seed;
            }
        }
    }

#if 0
    QLabel l;
    QImage img(MAP_SIZE, MAP_SIZE, QImage::Format_RGB32);
    OctaveNoiseField octave(4, seed);
    octave.setFalloff(0.5);
    octave.setInputScale(0.005);
    octave.setOutputScale(64);
    octave.setOutputOffset(128);
    QList<QPoint> boundary;
    bool lastValue = (octave.valueAt(0, 0) < 128);
    for(int y = 0; y < MAP_SIZE; y++) {
      for(int x = 0; x < MAP_SIZE; x++) {
        double v = octave.valueAt(x, y);
        bool value = (v < 128);
        if(value)
          img.setPixel(x, y, qRgb(0, 0, 0));
        else
          img.setPixel(x, y, qRgb(255, 255, 255));
        bool prevRowValue = (y > 0 ? octave.valueAt(x, y-1) : 0) < 128;
        if(value != lastValue || value != prevRowValue) {
          if((x % 8) == 4 && (y % 8) == 4)
            boundary << QPoint(x, y);
          img.setPixel(x, y, qRgb(0, 255, 0));
          lastValue = value;
        }
      }
    }
    std::unique(boundary.begin(), boundary.end());
    //std::sort(boundary.begin(), boundary.end(), [](const QPointF& lhs, const QPointF& rhs) -> bool { return lhs.x() < rhs.x(); });
    std::random_shuffle(boundary.begin(), boundary.end());
    QuadTree tree;
    for(const QPoint& pt : boundary) {
      tree.addPoint(pt.x(), pt.y());
    }
    QFile json("treevis.js");
    json.open(QIODevice::WriteOnly | QIODevice::Truncate);
    json.write("var tree = ");
    json.write(tree.toString().toUtf8());
    json.write(";\n");
    json.close();
    QImage img2 = img;
    for(int y = 200; y < 300; y++) {
      qDebug() << y;
      for(int x = 200; x < 300; x++) {
        QPointF nearest = tree.nearestNeighbor(x, y, 1);
        int dx = nearest.x() - x;
        int dy = nearest.y() - y;
        int r = std::sqrt(dx * dx + dy * dy);
        if(octave.valueAt(x, y) > 128)
          r = 144 + r;
        else
          r = 112 - r;
        img.setPixel(x, y, qRgb(r, r, r));
      }
    }
    l.setPixmap(QPixmap::fromImage(img));
    l.show();
    QLabel l2;
    l2.setPixmap(QPixmap::fromImage(img2));
    l2.show();
#endif



    MapGenerator map(seed, numContinents);

    QLabel l;
    if(showDebug) {
        l.setPixmap(QPixmap::fromImage(map.debugOverlay()));
    } else {
        l.setPixmap(QPixmap::fromImage(map.biomeMap(true, true)));
    }
    if(doSave) {
        l.pixmap()->save("map-" + QString::number(seed) + ".png");
        QFile dat("map-" + QString::number(seed) + ".dat");
        dat.open(QIODevice::WriteOnly | QIODevice::Truncate);
        QDataStream stream(&dat);
        stream << MAP_SIZE << MAP_SIZE;
        for(int y = 0; y < MAP_SIZE; y++) {
          for(int x = 0; x < MAP_SIZE; x++) {
            stream << map.altitudeAt(x, y) << (quint8)map.biomeAt(x, y);
          }
        }
    }
    l.show();

    /*
    QxtHttpSessionManager sm;
    MapService service(0, &sm);
    sm.setConnector(QxtHttpSessionManager::Scgi);
    sm.setPort(4000);
    sm.setAutoCreateSession(false);
    sm.setStaticContentService(&service);
    if(!sm.start()) {
        qFatal("SCGI service failed to start");
    }
    */

    return app.exec();
}
