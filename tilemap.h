#ifndef TILEMAP_H
#define TILEMAP_H

#include "mapgenerator.h"
#include <QVector>

#define B_NW 0b1000
#define B_NE 0b0100
#define B_SW 0b0010
#define B_SE 0b0001

enum TileShape {
    TS_Solid        = 0b1111,
    TS_InnerSE      = 0b1110,
    TS_InnerSW      = 0b1101,
    TS_EdgeN        = 0b1100,
    TS_InnerNE      = 0b1011,
    TS_EdgeW        = 0b1010,
    TS_DiagonalDown = 0b1001,
    TS_OuterNW      = 0b1000,
    TS_InnerNW      = 0b0111,
    TS_DiagonalUp   = 0b0110,
    TS_EdgeE        = 0b0101,
    TS_OuterNE      = 0b0100,
    TS_EdgeS        = 0b0011,
    TS_OuterSW      = 0b0010,
    TS_OuterSE      = 0b0001,
    TS_Transparent  = 0b0000,
    TS_Cliff1       = 16,
    TS_Cliff2       = 17,
    TS_Cliff3       = 18,
    TS_Cliff4       = 19,
    TS_Cliff5       = 20,
    TS_Cliff6       = 21,
};

class TileMap
{
public:
    TileMap(MapGenerator* map);

    QByteArray tileData() const;

private:
    struct Cell {
        BiomeType biome;
        union {
            unsigned char bits;
            TileShape shape;
        };
        unsigned char cliffBits() const;
        Cell* below;

        Cell(BiomeType _biome = Ocean, TileShape _shape = TS_Transparent);
        ~Cell();
    };
    BiomeType biomes[MAP_SIZE-2][MAP_SIZE-2];
    QVector<QVector<QVector<Cell>>> layers;
    QVector<QVector<QVector<qint16>>> flatness;
    QVector<qint16> maxFlatness;

    void setTile(int x, int y, int z, BiomeType biome, quint8 bits = TS_Solid);
    void cleanEdges();
    void calculateSteepness();
    void fillCliffs();
};

#endif
