#ifndef MAPSERVICE_H
#define MAPSERVICE_H

#include <QxtAbstractWebService>
#include <QHash>
#include <QTimer>
#include <QUrl>
#include <QImage>
#include <QQueue>
class QThread;
class MapGenerator;
class TileMap;

class MapService : public QxtAbstractWebService
{
Q_OBJECT
public:
    MapService(int sessionID, QxtAbstractWebSessionManager* sm);

protected:
    void pageRequestedEvent(QxtWebRequestEvent* event);

private slots:
    void expireWorlds();
    void dispatchPending();

private:
    struct World {
        World() : lastAccess(0), seed(0), map(nullptr), tiles(nullptr), tilesPending(false) {}
        ~World();
        int lastAccess;
        int seed;
        MapGenerator* map;
        QImage mapImage;
        TileMap* tiles;
        bool tilesPending;
        struct QueuedRequest {
            int sessionID, requestID;
            QUrl url;
        };
        QQueue<QueuedRequest> queue;
    };
    QHash<quint64, World> maps;
    QTimer expiryTimer;

    void dispatchRequest(int seed, int sessionID, int requestID, const QUrl& url);
};

#endif
