#include "octavenoisefield.h"
#include "aggregatefield.h"
#include "simplexnoisefield.h"

OctaveNoiseField::OctaveNoiseField(int octaves, int seed) : sum(AggregateField::create([](double accum, double value) { return accum + value; }))
{
    for(int i = 0; i < octaves; i++) {
        sum->add(new SimplexNoiseField(seed * (i+1)));
    }
    setFalloff(0.5);
}

SimplexNoiseField* OctaveNoiseField::first() const
{
    return static_cast<SimplexNoiseField*>(sum->children[0]);
}

double OctaveNoiseField::falloff() const
{
    return _falloff;
}

void OctaveNoiseField::setFalloff(double falloff)
{
    _falloff = falloff;

    int ct = sum->children.size();
    if(ct == 1) return;

    double iscale = inputScale();
    double oscale = outputScale();

    for(int i = 1; i < ct; i++) {
        iscale /= 0.5;
        oscale *= falloff;

        SimplexNoiseField* child = static_cast<SimplexNoiseField*>(sum->children[i]);
        child->setInputScale(iscale);
        child->setOutputScale(oscale);
        child->setOutputOffset(0.0);
    }
}

double OctaveNoiseField::inputScale() const
{
    return first()->inputScale();
}

void OctaveNoiseField::setInputScale(double iscale)
{
    first()->setInputScale(iscale);
    setFalloff(_falloff);
}

double OctaveNoiseField::outputScale() const
{
    return first()->outputScale();
}

void OctaveNoiseField::setOutputScale(double iscale)
{
    first()->setOutputScale(iscale);
    setFalloff(_falloff);
}

double OctaveNoiseField::outputOffset() const
{
    return first()->outputOffset();
}

void OctaveNoiseField::setOutputOffset(double offset)
{
    first()->setOutputOffset(offset);
}

double OctaveNoiseField::valueAt(double X, double Y) const
{
    return sum->valueAt(X, Y);
}
