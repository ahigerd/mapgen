#include "mapgenerator.h"
#include "octavenoisefield.h"
#include "linedistancefield.h"
#include <cmath>
#include <QPainter>
#include <QLineF>
#include <QSet>
#include <QtDebug>
#include <QDataStream>
#include <QFile>
#include <QDir>

#define FLOAT_IT_2(k) k ## .0
#define FLOAT_IT(k) FLOAT_IT_2(k)
#define FSIZE FLOAT_IT(MAP_SIZE)
#define HALFSIZE (MAP_SIZE/2)
static const double PI = 3.141592653589793238463;
static const double SQRT2 = 1.414; // I know it's not really, but it looks good
static const QRgb _undef = qRgb(0, 0, 0);
static const QRgb _land = qRgb(255, 255, 255);
static const QRgb _ocean = qRgb(1, 1, 128);
static const QRgb _river = qRgb(2, 2, 128);
static const QRgb _slope[9] = {
    qRgb(32, 32, 32),
    qRgb(64, 64, 64),
    qRgb(96, 96, 96),
    qRgb(128, 128, 128),
    qRgb(160, 160, 160),
    qRgb(192, 192, 192),
    qRgb(224, 224, 224),
    qRgb(255, 255, 255),
    qRgb(0, 0, 0)
};
static const int SCALE = 3;

static const int dirs[][2] = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };

static const int slopes[][2] = {
    { -1, -1 },
    {  0, -1 },
    { -1,  0 },
    { +1, -1 },
    { -1, +1 },
    { +1,  0 },
    {  0, +1 },
    { +1, +1 }
};

static const int temperatureLevels[] = {
    48,
    92,
    160,
    255
};

static const int moistureLevels[] = {
    70,
    92,
    115,
    125,
    150,
    155
};

static inline bool isLake(QRgb rgb)
{
    return (rgb & 0xFF0000) != 0;
}

int MapGenerator::colorDistance(QRgb rgb)
{
    if(rgb == _ocean) return 0;
    if(rgb == _land) return -1;
    if(rgb == _undef) return -2;
    int r = (rgb & 0xFF0000) >> 16;
    int g = (rgb & 0x00FF00) >> 8;
    int b = (rgb & 0x0000FF);
    if(r != 0) {
        return g * 256 + b - 96;
    } else {
        return b * 256 + g - 96;
    }
}

QRgb MapGenerator::distanceColor(int dist, bool water)
{
    dist += 96;
    if(water) {
        return qRgb(1, dist/256, dist%256);
    } else {
        return qRgb(0, dist%256, dist/256);
    }
}

MapGenerator::MapGenerator(int _seed, int numContinents)
: _seed(_seed), numContinents(numContinents),
  img(MAP_SIZE, MAP_SIZE, QImage::Format_RGB32)
{
    if(QFile::exists(QString("maps/%1/map.dat").arg(_seed))) {
        qDebug() << "restoring from save";
        img = QImage(QString("maps/%1/map.png").arg(_seed));
        QFile dat(QString("maps/%1/map.dat").arg(_seed));
        dat.open(QIODevice::ReadOnly);
        QDataStream stream(&dat);
        stream >> altitude >> moisture >> moistureNoise >> slope;
        return;
    }

    exclusionRadius = MAP_SIZE * (1.0 - 1.0768 / std::sqrt(numContinents));
    qDebug() << "Exclusion radius" << exclusionRadius;

    generateMountains();
    generateNoise();
    fillOcean();
    fillDistance();
    calculateSlopes();
    addRivers();
    calculateMoisture();

    QDir().mkpath(QString("maps/%1").arg(_seed));
    img.save(QString("maps/%1/map.png").arg(_seed));
    QFile dat(QString("maps/%1/map.dat").arg(_seed));
    dat.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QDataStream stream(&dat);
    stream << altitude << moisture << moistureNoise << slope;
    dat.close();
}

MapGenerator::~MapGenerator()
{
    delete noiseField;
}

int MapGenerator::seed() const
{
    return _seed;
}

QImage MapGenerator::map() const
{
    return img;
}

void MapGenerator::saveMap(const QString& filename)
{
    img.save(filename);
}

QImage MapGenerator::debugOverlay() const
{
    double sectorSize = 2*PI/numContinents;
    double sectorOffset = AbstractField::hashFunction(_seed, _seed, _seed) % 1000 / 1000.0 * sectorSize;

    QImage overlay = img;
    QPainter p(&overlay);
    for(auto range : ranges) {
        p.setPen(QColor(range[4], 0, 0));
        int x1 = range[0]/SCALE;
        int y1 = range[1]/SCALE;
        int x2 = range[2]/SCALE;
        int y2 = range[3]/SCALE;
        p.drawLine(x1 + HALFSIZE, y1 + HALFSIZE, x2 + HALFSIZE, y2 + HALFSIZE);
    }
    if(numContinents > 1) {
        for(int c = 0; c < numContinents; c++) {
            p.setPen(QColor(0, 0, 255));
            double theta = c * sectorSize + sectorOffset;
            p.drawLine(HALFSIZE, HALFSIZE, HALFSIZE + std::cos(theta) * MAP_SIZE, HALFSIZE + std::sin(theta) * MAP_SIZE);
            p.setPen(QColor(0, 255, 0));
            theta = (c+0.5) * sectorSize + sectorOffset;
            p.drawLine(HALFSIZE, HALFSIZE, HALFSIZE + std::cos(theta) * MAP_SIZE, HALFSIZE + std::sin(theta) * MAP_SIZE);
        }
        int sz = exclusionRadius / SCALE;
        p.drawEllipse(QPoint(HALFSIZE, HALFSIZE), sz, sz);
    }
    p.setPen(QColor(0, 255, 255));
    foreach(const QPoint& river, riverStarts) {
        p.drawEllipse(river, 2, 2);
    }
    return overlay;
}

void MapGenerator::generateMountains()
{
    static const int NUM_RANGES = 2;

    mountains = AggregateField::create([](double a, double v) { return a + v; });

    double sectorSize = 2*PI/numContinents;
    double sectorOffset = AbstractField::hashFunction(_seed, _seed, _seed) % 1000 / 1000.0 * sectorSize;

    for(int c = 0; c < numContinents; c++) {
        double minTheta = c * sectorSize + sectorOffset;
        double maxTheta = (c+0.5) * sectorSize + sectorOffset;
        for(int i = 0; i < NUM_RANGES; i++) {
            bool ok = false;
            int x1, y1, x2, y2, attempt = 0;
            do {
                x1 = AbstractField::hashFunction(c*NUM_RANGES+i, attempt+0, _seed/2) % MAP_SIZE - (HALFSIZE);
                y1 = AbstractField::hashFunction(c*NUM_RANGES+i, attempt+1, _seed/2) % MAP_SIZE - (HALFSIZE);
                x2 = AbstractField::hashFunction(c*NUM_RANGES+i, attempt+2, _seed/2) % MAP_SIZE - (HALFSIZE);
                y2 = AbstractField::hashFunction(c*NUM_RANGES+i, attempt+3, _seed/2) % MAP_SIZE - (HALFSIZE);
                x1 *= 2; y1 *= 2; x2 *= 2; y2 *= 2;

                if(numContinents > 1) {
                    if(x1*x1+y1*y1 < exclusionRadius*exclusionRadius || x2*x2+y2*y2 < exclusionRadius*exclusionRadius) {
                        attempt += 4;
                        continue;
                    }
                    double theta = std::atan2(y1, x1);
                    if(theta < sectorOffset) theta += 2*PI;
                    if(theta < minTheta || theta > maxTheta) {
                        attempt += 4;
                        continue;
                    }
                    theta = std::atan2(y2, x2);
                    if(theta < 0) theta += 2*PI;
                    if(theta < minTheta || theta > maxTheta) {
                        attempt += 4;
                        continue;
                    }
                }
                ok = true;
                QLineF line1(x1, y1, x2, y2);
                for(auto range : ranges) {
                    QLineF line2(range[0], range[1], range[2], range[3]);
                    if(line1.intersect(line2, nullptr) == QLineF::BoundedIntersection) {
                        ok = false;
                        attempt += 4;
                        break;
                    }
                }
            } while(!ok);
            int height = AbstractField::hashFunction(i, attempt + 4, _seed/2) % 256;
            ranges.push_back({ x1, y1, x2, y2, height });
            LineDistanceField* line = new LineDistanceField(x1, y1, x2, y2);
            line->setOutputThreshold(1.0 / 256.0 * height);
            line->setOutputScale(1.0 / 256.0 * height);
            mountains->add(line);
        }
    }
}

void MapGenerator::generateNoise()
{
    static const int THRESHOLD_MID = MAP_SIZE*MAP_SIZE/4;
    static const int THRESHOLD_MIN = THRESHOLD_MID / 1.5;
    static const int THRESHOLD_MAX = THRESHOLD_MID * 1.5;
    OctaveNoiseField* octave = new OctaveNoiseField(4, _seed);
    octave->setFalloff(0.5);
    octave->setInputScale(0.005);
    octave->setOutputScale(10);
    octave->setOutputOffset(0);

    moistureNoise.resize(MAP_SIZE*MAP_SIZE);
    for(int y = 0; y < MAP_SIZE; y++) {
        int sy = (y - HALFSIZE) * SCALE;
        for(int x = 0; x < MAP_SIZE; x++) {
            int sx = (x - HALFSIZE) * SCALE;
            moistureNoise[y*MAP_SIZE+x] = octave->valueAt(sx, sy);
        }
    }

    AggregateField* noise = AggregateField::create([](double a, double v) {
        double base = 1.2 / std::sqrt(a);
        double rv = (255 - base + 4*v - 40) / 255.0;
        if(rv < 0) return 0.0;
        return rv;
    });

    noise->add(mountains);
    noise->add(octave);

    OctaveNoiseField* octave2 = new OctaveNoiseField(4, _seed);
    octave2->setFalloff(0.5);
    octave2->setInputScale(0.004);
    octave2->setOutputScale(128);
    octave2->setOutputOffset(128);
    extraNoise = octave2;

    noiseField = nullptr;
    int threshold = 32;
    for(int iter = 0; iter < 5; iter++) {
        if(noiseField) {
            noiseField->setAutoDelete(false);
            delete noiseField;
        }
        noiseField = AggregateField::create([threshold](double a, double v) {
            if(a < 0.1) return 0.0;
            if(v < threshold) return 0.0;
            return 250.0;
        });
        noiseField->add(noise);
        noiseField->add(octave2);

        int pixelCount = 0;
        for(int x = -HALFSIZE; x < HALFSIZE; x++) {
            for(int y = -HALFSIZE; y < HALFSIZE; y++) {
                int v = noiseField->valueAt(x*SCALE, y*SCALE);
                if(v < threshold) {
                    v = 0;
                } else {
                    v = 255;
                    pixelCount++;
                }
                img.setPixel(x + HALFSIZE, y + HALFSIZE, qRgb(v, v, v));
            }
        }
        if(pixelCount < THRESHOLD_MIN) {
            threshold /= 2;
            qDebug() << pixelCount << THRESHOLD_MIN << threshold;
        } else if(pixelCount > THRESHOLD_MAX) {
            threshold *= 1.5;
            qDebug() << pixelCount << THRESHOLD_MIN << threshold;
        } else {
            qDebug() << pixelCount << THRESHOLD_MID << threshold;
            break;
        }
    }

    for(int i = 0; i < MAP_SIZE; i++) {
        img.setPixel(i, 0, 0);
        img.setPixel(0, i, 0);
        img.setPixel(i, MAP_SIZE-1, 0);
        img.setPixel(MAP_SIZE-1, i, 0);
    }
}

struct FillState { int x, y, dir; };
QVector<FillState> more;

void MapGenerator::fillOcean(int x0, int y0, int state)
{
    int tx = x0, ty = y0;
    while(tx >= 0 && ty >= 0 && tx < MAP_SIZE && ty < MAP_SIZE) {
        QRgb px = img.pixel(tx, ty);
        if(px != _undef) return;
        img.setPixel(tx, ty, _ocean);
        tx += dirs[state][0];
        ty += dirs[state][1];
        for(int i = 0; i < 4; i++) {
            if(i == state || i == state + 2 || i == state - 2) continue;
            more << FillState({ tx + dirs[i][0], ty + dirs[i][1], i });
        }
    }
    while(!more.isEmpty()) {
        FillState m = more.last();
        more.pop_back();
        fillOcean(m.x, m.y, m.dir);
    }
}

static QVector<int> lakePoints;
int MapGenerator::fillLake(int x0, int y0, float height, int state, int x00, int y00)
{
    if(x00 == -1) {
        lakePoints.clear();
        x00 = x0;
        y00 = y0;
        if(!lakeExitPoints.contains(QPoint(x00, y00))) {
            bool addPoint = true;
            for(int yi = y0-1; yi <= y0+1; yi++) {
                for(int xi = x0-1; xi <= x0+1; xi++) {
                    QRgb px = img.pixel(xi, yi);
                    if(px == _ocean || px == _river || !lakeExits[yi*MAP_SIZE+xi].isNull()) {
                        addPoint = false;
                        break;
                    }
                }
            }
            if(addPoint)
                lakeExitPoints << QPoint(x00, y00);
        }
    }
    int filled = 0;
    int tx = x0, ty = y0;
    while(tx >= 0 && ty >= 0 && tx < MAP_SIZE && ty < MAP_SIZE) {
        QRgb px = img.pixel(tx, ty);
        if(px != _undef) break;
        if(altitude[y0*MAP_SIZE+x0] != -1) break;
        altitude[ty*MAP_SIZE+tx] = height;
        lakePoints << ty*MAP_SIZE+tx;
        lakeExits[ty*MAP_SIZE+tx] = QPoint(x00, y00);
        filled++;
        tx += dirs[state][0];
        ty += dirs[state][1];
        for(int i = 0; i < 4; i++) {
            if(i == state || i == state + 2 || i == state - 2) continue;
            more << FillState({ tx + dirs[i][0], ty + dirs[i][1], i });
        }
    }
    while(!more.isEmpty()) {
        FillState m = more.last();
        more.pop_back();
        filled += fillLake(m.x, m.y, height, m.dir, x00, y00);
    }
    return filled;
}

void MapGenerator::fillDistance()
{
    altitude.resize(MAP_SIZE*MAP_SIZE);
    lakeExits.resize(MAP_SIZE*MAP_SIZE);
    lakeSize.resize(MAP_SIZE*MAP_SIZE);
    int minX = 1, minY = 1, maxX = MAP_SIZE-2, maxY = MAP_SIZE-2;
    int changed;
    int iter = 2;

    for(int y = 0; y < MAP_SIZE; y++) {
        for(int x = 0; x < MAP_SIZE; x++) {
            QRgb px = img.pixel(x, y);
            int d = colorDistance(px);
            if(d == 0) {
                altitude[y*MAP_SIZE+x] = 0;
            } else {
                altitude[y*MAP_SIZE+x] = -1;
            }
        }
    }
    do {
        int nminX = MAP_SIZE-1, nminY = MAP_SIZE-1, nmaxX = 0, nmaxY = 0;
        changed = 0;
        for(int y = minY; y <= maxY; y++) {
            int sy = (y - HALFSIZE) * SCALE;
            for(int x = minX; x <= maxX; x++) {
                int sx = (x - HALFSIZE) * SCALE;
                int v2 = 128 - 1.2 / std::sqrt(mountains->valueAt(sx, sy));

                float dist = altitude[y*MAP_SIZE+x];
                if(dist >= 0) continue;

                float closest = MAP_SIZE*MAP_SIZE;
                for(int yi = y-1; yi <= y+1; yi++) {
                    for(int xi = x-1; xi <= x+1; xi++) {
                        if(yi == y && xi == x) continue;
                        float d = altitude[yi*MAP_SIZE+xi];
                        if(d < 0) continue;
                        if(xi != x && yi != y)
                            d += SQRT2;
                        else
                            d += 1;
                        if(d < closest) {
                            closest = d;
                        }
                    }
                }
                if(closest == MAP_SIZE*MAP_SIZE) continue; // No changes

                if(v2 > 0)
                    closest += v2 / 30.0;
                double noise = extraNoise->valueAt(sx, sy) / 30.0;
                if(closest - noise > 0)
                    closest += noise;

                if(x < nminX) nminX = x;
                if(x > nmaxX) nmaxX = x;
                if(y < nminY) nminY = y;
                if(y > nmaxY) nmaxY = y;
                changed++;
                if(closest > iter) continue;  // But it's too soon

                bool wasFilled = (altitude[y*MAP_SIZE+x] >= 0);
                if(img.pixel(x,y) == _undef) {
                    // This is a lake. Modify the whole thing if it hasn't already been done.
                    if(!wasFilled) {
                        fillLake(x, y, closest);
                        int numPoints = lakePoints.size();
                        if(numPoints > 32) {
                            foreach(int lp, lakePoints) {
                                lakeSize[lp] = numPoints;
                            }
                        }
                    }
                } else {
                    altitude[y*MAP_SIZE+x] = closest;
                }
            }
        }
        ++iter;
        minX = nminX;
        minY = nminY;
        maxX = nmaxX;
        maxY = nmaxY;
    } while(changed);

    for(int y = 0; y < MAP_SIZE; y++) {
        for(int x = 0; x < MAP_SIZE; x++) {
            int dist = altitude[y*MAP_SIZE+x];
            if(dist == 0) continue;
            if(img.pixel(x,y) != _undef) {
                img.setPixel(x, y, distanceColor(dist));
            } else {
                img.setPixel(x, y, distanceColor(dist, true));
            }
        }
    }
}

void MapGenerator::calculateSlopes()
{
    slope.resize(MAP_SIZE*MAP_SIZE);
    for(int y = 1; y < MAP_SIZE-1; y++) {
        for(int x = 1; x < MAP_SIZE-1; x++) {
            if(slope[y*MAP_SIZE+x] != 0) continue;
            QRgb px = altitude[y*MAP_SIZE+x];
            if(px < 1) {
                slope[y*MAP_SIZE+x] = 0xFF;
                continue;
            }
            double dists[8] = {
                altitude[(y-1)*MAP_SIZE+x-1] * 1.1,
                altitude[(y-1)*MAP_SIZE+x],
                altitude[y*MAP_SIZE+x-1],
                altitude[(y-1)*MAP_SIZE+x+1] * 1.1,
                altitude[(y+1)*MAP_SIZE+x-1] * 1.1,
                altitude[y*MAP_SIZE+x+1],
                altitude[(y+1)*MAP_SIZE+x],
                altitude[(y+1)*MAP_SIZE+x+1] * 1.1
            };
            int highDist = 9, highVal = 0;
            for(int i = 0; i < 8; i++) {
                if(dists[i] > highVal) {
                    highDist = i;
                    highVal = std::fabs(dists[i]);
                }
            }
            slope[y*MAP_SIZE+x] = highDist;
        }
    }
}

void MapGenerator::addRivers()
{
    int NUM_RIVERS = 10;
    for(int i = 0; i < NUM_RIVERS; i++) {
        int attempt = 0;
        int x, y;
        do {
            x = AbstractField::hashFunction(i, attempt+0, _seed/3) % MAP_SIZE;
            y = AbstractField::hashFunction(i, attempt+1, _seed/3) % MAP_SIZE;
            attempt += 2;
            QRgb px = img.pixel(x, y);
            if(px == _ocean || px == _river) continue;
            bool ok = true;
            foreach(const QPoint& river, riverStarts) {
                if(QLineF(x, y, river.x(), river.y()).length() < 50) {
                    ok = false;
                    break;
                }
            }
            if(!ok) continue;
            if(altitude[y*MAP_SIZE+x] < 200) continue;
            break;
        } while(attempt < 40);
        if(attempt >= 40) continue;
        addRiver(x, y);
    }
    foreach(const QPoint& ex, lakeExitPoints) {
        if(riverStarts.contains(ex)) continue;
        int x = ex.x(), y = ex.y();
        bool doAdd = true;
        for(int yi = y-1; yi <= y+1; yi++) {
            for(int xi = x-1; xi <= x+1; xi++) {
                QRgb px = img.pixel(x, y);
                if(px == _ocean || px == _river) {
                    doAdd = false;
                    break;
                }
            }
        }
        if(doAdd) addRiver(x, y);
    }
}

void MapGenerator::addRiver(int x, int y)
{
    QPainter p(&img);
    p.setPen(_river);
    p.setBrush(QColor(_river));

    riverStarts << QPoint(x, y);
    QSet<int> history;
    int lastDX = 0, lastDY = 0, straightLen = 0, len = 0;
    do {
        len++;
        history << ((x << 16) | y);
        QPoint ex = lakeExits[y*MAP_SIZE+x];
        if(!ex.isNull()) {
            x = ex.x();
            y = ex.y();
            straightLen = 0;
            p.setPen(img.pixel(x, y));
            p.setBrush(QColor(img.pixel(x, y)));
        } else {
            p.setPen(_river);
            p.setBrush(QColor(_river));
            img.setPixel(x, y, _river);
        }
        double size = std::sqrt(len / 20.0);
        if(size < 1.2) size = 1.2;
        else if(size > 4) size = 4;
        p.drawEllipse(QPointF(x + 0.5, y + 0.5), size, size);

        int lowX = 0, lowY = 0, lowZ = MAP_SIZE;
        for(int dy = -1; dy <= 1; dy++) {
            int yi = y + dy;
            for(int dx = -1; dx <= 1; dx++) {
                if(dx == 0 && dy == 0) continue;
                int xi = x + dx;
                if(straightLen > 2 && dx == lastDX && dy == lastDY) continue;
                if(history.contains((xi << 16) | yi)) continue;
                int z = altitude[yi*MAP_SIZE+xi];
                if(z < lowZ) {
                    lowX = xi;
                    lowY = yi;
                    lowZ = z;
                }
            }
        }

        if(lowZ < 1 || lowZ == MAP_SIZE) {
            // Go one step further to help smooth things out
            p.drawEllipse(QPointF(x + lastDX + 0.5, y + lastDY + 0.5), size + .5, size + .5);
            break;
        }

        int dx = lowX - x;
        int dy = lowY - y;
        if(dx == lastDX && dy == lastDY) {
            straightLen++;
        } else {
            straightLen = 0;
            lastDX = dx;
            lastDY = dy;
        }

        x = lowX;
        y = lowY;
    } while(x > 0 && y > 0 && x < MAP_SIZE-1 && y < MAP_SIZE-1);
}

QImage MapGenerator::slopeMap(bool blend) const
{
    QImage s(MAP_SIZE, MAP_SIZE, QImage::Format_RGB32);
    for(int y = 0; y < MAP_SIZE; y++) {
        for(int x = 0; x < MAP_SIZE; x++) {
            if(img.pixel(x, y) == _ocean) {
                s.setPixel(x, y, _ocean);
            } else if(!blend) {
                s.setPixel(x, y, _slope[slope[y*MAP_SIZE+x]]);
            } else {
                int intensity = 0;
                for(int yi = -2; yi <= 2; yi++) {
                    for(int xi = -2; xi <= 2; xi++) {
                        if(yi == 0 && xi == 0) continue;
                        int px = _slope[slope[(y+yi)*MAP_SIZE+x+xi]] & 0xFF;
                        intensity += px;
                    }
                }
                intensity /= 24;
                s.setPixel(x, y, qRgb(intensity, intensity, intensity));
            }
        }
    }
    return s;
}

void MapGenerator::calculateMoisture()
{
    moisture.resize(MAP_SIZE*MAP_SIZE);
    int changed;
    int iter = 254;

    for(int y = 0; y < MAP_SIZE; y++) {
        for(int x = 0; x < MAP_SIZE; x++) {
            QRgb px = img.pixel(x, y);
            if(px != _ocean && (isLake(px) || px == _river)) {
                // make sure that we don't pick single freshwater points
                // correct them if they're isolated
                int numFresh = 0, numOcean = 0;
                for(int yi = y-1; yi <= y+1; yi++) {
                    for(int xi = x-1; xi <= x+1; xi++) {
                        QRgb pxi = img.pixel(xi, yi);
                        if(pxi == _ocean) {
                            numOcean++;
                        } else if(isLake(pxi) || pxi == _river) {
                            numFresh++;
                        }
                    }
                }
                if(px == _river) {
                    moisture[y*MAP_SIZE+x] = 130;
                } else if(numFresh >= 2) {
                    moisture[y*MAP_SIZE+x] = 130 + lakeSize[y*MAP_SIZE+x] / 8;
                } else if(numOcean > 0) {
                    moisture[y*MAP_SIZE+x] = -1;
                    altitude[y*MAP_SIZE+x] = 0;
                    img.setPixel(x, y, _ocean);
                    qDebug() << "isolated freshwater A:" << x << y;
                } else {
                    moisture[y*MAP_SIZE+x] = -1;
                    img.setPixel(x, y, distanceColor(altitude[y*MAP_SIZE+x]));
                    qDebug() << "isolated freshwater B:" << x << y;
                }
            } else {
                moisture[y*MAP_SIZE+x] = -1;
            }
        }
    }

    do {
        changed = 0;
        for(int y = 1; y < MAP_SIZE-1; y++) {
            for(int x = 1; x < MAP_SIZE-1; x++) {
                float dist = moisture[y*MAP_SIZE+x];
                if(dist >= iter) continue;

                float closest = 0, cx = x, cy = y;
                float alt0 = altitude[y*MAP_SIZE+x];
                float altMult = (alt0 > 300) ? 0.8 : 1.0;
                for(int yi = y-1; yi <= y+1; yi++) {
                    for(int xi = x-1; xi <= x+1; xi++) {
                        if(yi == y && xi == x) continue;
                        float d = moisture[yi*MAP_SIZE+xi];
                        if(d < 0) continue;
                        if(xi != x && yi != y)
                            d -= SQRT2 * altMult;
                        else
                            d -= altMult;
                        if(d > closest) {
                            closest = d;
                            cx = xi;
                            cy = yi;
                        }
                    }
                }
                if(closest == 0) continue; // No changes

                float alt1 = altitude[cy*MAP_SIZE+cx];
                if(alt0 < 300 && alt1 > alt0) closest *= 1.0 - (0.002 * (alt1-alt0));

                changed++;
                if(closest < iter) continue;  // But it's too soon

                moisture[y*MAP_SIZE+x] = closest;
            }
        }
        --iter;
    } while(changed);
}

int MapGenerator::moistureAt(int x, int y) const
{
    if(x == 0 || y == 0 || x > MAP_SIZE-2 || y > MAP_SIZE-2) return 255 - moisture[y*MAP_SIZE+x];
    int rv = moisture[y*MAP_SIZE+x] + moistureNoise[y*MAP_SIZE+x];
    if(rv < 0) return 0;
    if(rv > 255) return 255;
    return rv;
}

QImage MapGenerator::moistureMap(bool quantize) const
{
    QImage m(MAP_SIZE, MAP_SIZE, QImage::Format_RGB32);
    for(int y = 0; y < MAP_SIZE; y++) {
        for(int x = 0; x < MAP_SIZE; x++) {
            int t = moistureAt(x, y);
            if(quantize) {
                for(int i = 0; i < 6; i++) {
                    if(t <= moistureLevels[i]) {
                        m.setPixel(x, y, qRgb(moistureLevels[i], moistureLevels[i], moistureLevels[i]));
                        break;
                    }
                }
            } else {
                m.setPixel(x, y, distanceColor(t));
            }
        }
    }
    return m;
}

int MapGenerator::temperatureAt(int x, int y) const
{
    double lat = 1 - std::pow(std::fabs(y - HALFSIZE) / (FSIZE / 2.0), 2);
    double alt = 1 - std::pow(altitude[y*MAP_SIZE+x] / 320.0, 2);
    int temp = lat * 171.0 + alt * 128.0 - 140;
    if(temp < 0) return 0;
    return temp;
}

QImage MapGenerator::temperatureMap(bool quantize) const
{
    QImage m(MAP_SIZE, MAP_SIZE, QImage::Format_RGB32);
    for(int y = 0; y < MAP_SIZE; y++) {
        for(int x = 0; x < MAP_SIZE; x++) {
            int t = temperatureAt(x, y);
            if(quantize) {
                for(int i = 0; i < 4; i++) {
                    if(t <= temperatureLevels[i]) {
                        m.setPixel(x, y, qRgb(temperatureLevels[i], temperatureLevels[i], temperatureLevels[i]));
                        break;
                    }
                }
            } else {
                m.setPixel(x, y, distanceColor(t));
            }
        }
    }
    return m;
}

int MapGenerator::biomeAt(int x, int y) const
{
    int tile = tileAt(x, y);
    int t = temperatureAt(x, y);
    if(tile == TT_Ocean) {
        if(t > temperatureLevels[1]) return Ocean;
        double thresh = std::sqrt(t) * 1.1 + 0.1;
        int mn = std::abs(moistureNoise[y*MAP_SIZE+x]) / thresh;
        if(mn > 0) return Snow;
        return Ocean;
    }
    if(tile == TT_River) return River;
    if(tile == TT_Lake) return Lake;
    int m = moistureAt(x, y);
    if(altitude[y*MAP_SIZE+x] < 6) {
        if(t < temperatureLevels[1] * .6) return Snow;
        if(t > temperatureLevels[1] * .8) return Beach;
        double thresh = std::sqrt(t) + 0.1;
        int mn = std::abs((double)m) / thresh;
        if(mn > 0 ) return Snow;
        else return Beach;
    } else if(t < temperatureLevels[0]) {
        if(m > moistureLevels[2]) return Snow;
        else if(m > moistureLevels[3]) return Plains;
        else return Bare;
    } else if(t < temperatureLevels[1]) {
        if(m > moistureLevels[1]) return Plains;
        else return Desert;
    } else if(t < temperatureLevels[2]) {
        if(m > moistureLevels[3]) return Forest;
        else if(m > moistureLevels[1]) return Plains;
        else return Desert;
    } else {
        if(m > moistureLevels[1]) return Forest;
        else if(m > moistureLevels[0]) return Plains;
        else return Desert;
    }
}

QImage MapGenerator::biomeMap(bool slope, bool blend) const
{
    QImage slopeImg;
    if(slope) slopeImg = slopeMap(blend);
    static const QRgb _biomes[] = {
        qRgb(0, 0, 192),        // Ocean
        qRgb(0, 128, 0),        // Forest
        qRgb(255, 255, 255),    // Snow
        qRgb(0, 255, 0),        // Plains
        qRgb(255, 255, 0),      // Desert
        qRgb(192, 192, 192),    // Beach
        qRgb(128, 128, 128),    // Bare
        qRgb(0, 0, 224),        // Lake
        qRgb(0, 0, 255),        // River

    };
    QImage m(MAP_SIZE, MAP_SIZE, QImage::Format_RGB32);
    for(int y = 0; y < MAP_SIZE; y++) {
        for(int x = 0; x < MAP_SIZE; x++) {
            QRgb rgb = _biomes[biomeAt(x, y)];
            m.setPixel(x, y, rgb);
            if(!slope) continue;

            QColor s(slopeImg.pixel(x, y));
            if(s.blue() != s.red()) continue;

            QColor c = QColor::fromRgb(rgb);
            if(c.red() < 2 && c.green() < 2) continue;
            double alt = std::pow(altitudeAt(x, y) / 60.0, 1.3) * 4.0;
            double factor = s.blue() / 255.0 * 26.0 - 13.0;
            factor *= 0.8 + (alt * 0.15);
            m.setPixel(x, y, c.lighter(factor + 75).rgb());
        }
    }
    return m;
}

int MapGenerator::pixelAt(int x, int y) const
{
    if(x < 0 || x >= MAP_SIZE || y < 0 || y >= MAP_SIZE) return _ocean;
    return img.pixel(x, y);
}

float MapGenerator::altitudeAt(int x, int y) const
{
    return altitude[y*MAP_SIZE+x];
}

int MapGenerator::tileAt(int x, int y) const
{
    QRgb px = pixelAt(x, y);
    if(px == _ocean)
        return TT_Ocean;
    else if(px == _river)
        return TT_River;
    else if(isLake(px))
        return TT_Lake;
    else
        return TT_Land;
}
